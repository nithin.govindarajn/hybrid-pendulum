function [ p0bar,Tkick] = EulerSolver_Nithin(ODEfun, p0, theta_kick,k, h  )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


f = ODEfun;



t = 0;
y = [-theta_kick; p0];


% integrate freely until angle changes sign
while y(1)<= 0

    y = y + h*f(t,y,k); 
    t = t + h;
    
end

% integrate until angle changes sign again (somewhere in between
% it should have reached theta_kick if sufficient momemtum was there) 
p0bar = NaN;
 Tkick = NaN;
while y(1)>= 0

    yprev = y;
    y = y + h*f(t,y,k); 
    t = t + h;
    
    
    if y(1)>theta_kick
        p0bar = (y(2) + yprev(2))/2; 
        Tkick = t-0.5*h;
        break;
    end
    
    
end





end

