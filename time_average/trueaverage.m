clc
clear all
close all

%  pendulum parameters
global g l m
g = 9.81;
l = 10;
m = 1;

% discrete part: momentum change
global theta_left theta_right dp_left dp_right
theta_left  = -pi/3; 
theta_right = pi/3;
dp_left = 1;
dp_right = -1;


global H V T

% Hamiltonian function
H = @(theta, omega)(0.5*m *l^2*omega.^2 - m*g*l*cos(theta) +m *g*l);
% Potential function
V = @(theta)( m*g*l*(1-cos(theta)) );
% Kinetic function
T = @(omega)(0.5*m^l^2* omega.^2);


%Some important constants
global Pcrit Hmax
Pcrit = V(theta_left);
Hmax = H(pi,0);

[omegalist,tlist] = computeweights(max(dp_left,dp_right));
%
[THETA0, OMEGA0] = meshgrid(-pi:0.05*pi:pi, -3:0.05:3);
[M,N] = size(THETA0);
Htimeavg = zeros(size(THETA0));
taus = zeros(M,N,2);
omegas = zeros(M,N,2);
qmat = zeros(M,N);
thetamat = zeros(M,N);
baseper = 2*pi*sqrt(g/l);

for jj= 1:M
    for kk = 1:N
        [omegas(jj,kk,:),qmat(jj,kk),thetamat(jj,kk)] = computelimomega( THETA0(jj,kk), OMEGA0(jj,kk) );
        if (qmat(jj,kk) == 0) || (qmat(jj,kk) == 3)
            Htimeavg(jj,kk) = H(omegas(jj,kk,1),thetamat(jj,kk));
        else
            for mm = 1:2
                inter = true;
                o1 = abs(omegas(jj,kk,mm));
                ocomp = omegalist-o1;
                ind = 1;
%                 [T1,Y] = ode45(@dypend,[0,2*baseper],[theta_left,o1]);
%                 Yuse = Y(:,1);
%                 Yuse = Yuse - theta_right;
%                 ind = 2;
%                 while Yuse(ind) < 0
%                     ind = ind + 1;
%                     if ind == length(Yuse)
%                         [val,ind] = min(Yuse(2:end));
%                         ind = ind+1;
%                         inter = false;
%                         %taus(jj,kk,mm) = quadcalc(T1(ind-1:ind+1),Yuse(ind-1:ind+1));
%                         break
%                     end
%                 end
                if inter ~= false
                    per = interp1(Yuse(ind-1:ind),T1(ind-1:ind),0);
                    taus(jj,kk,mm) = per;
                end
            end
            H1 = H(omegas(jj,kk,1),theta_left);
            H2 = H(omegas(jj,kk,2),theta_left);
            Htimeavg(jj,kk) = (H1*taus(jj,kk,1)+H2*taus(jj,kk,2))/(taus(jj,kk,1)+taus(jj,kk,2));
        end
    end
end


figure(3)
contourf(THETA0, OMEGA0, Htimeavg ,100,'LineStyle','None')
title('Time average of Observable')
caxis([60 100])
colormap jet
xlabel('\theta'); ylabel('\omega')
colorbar
% 
% figure(4)
% contourf(THETA0, OMEGA0, H(THETA0, OMEGA0) ,100,'LineStyle','None')
% 
% xlabel('\theta'); ylabel('\omega')
% colorbar